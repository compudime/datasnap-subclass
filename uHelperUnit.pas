unit uHelperUnit;

interface

type
  THelperUnit = class
  private
    FInLoop: Boolean;
  public
    procedure WaitSomeTime(ASeconds: Integer);
  end;

implementation

uses
  ServerMethodsUnit1;

{ THelperUnit }

procedure THelperUnit.WaitSomeTime(ASeconds: Integer);
begin
  MyHelperUnit2.WaitSomeTime(ASeconds);
end;

end.
