unit uHelperUnit2;

interface

type
  THelperUnit2 = class
  private
    FInLoop: Boolean;
    FMyVar: string;
  public
    property MyVar: string read FMyVar;
    procedure WaitSomeTime(ASeconds: Integer);
    procedure WaitAndEcho(AEchoValue: string; AWaitSeconds: Integer);
    constructor Create;
  end;

implementation

uses
  System.SysUtils, Forms;

{ THelperUnit }

constructor THelperUnit2.Create;
begin
  FMyVar := 'Unassigned';
end;

procedure THelperUnit2.WaitAndEcho(AEchoValue: string; AWaitSeconds: Integer);
var
  I: Integer;
begin
  FMyVar := AEchoValue;
  I := 0;
  repeat
    Sleep(1000);
    Application.ProcessMessages;
    Inc(I);
  until I >= AWaitSeconds;
end;

procedure THelperUnit2.WaitSomeTime(ASeconds: Integer);
var
  I: Integer;
begin
  if FInLoop then
    raise Exception.Create('Already In Loop');

  FInLoop := True;
  try
  I := 0;
  repeat
    Sleep(1000);
    Application.ProcessMessages;
    Inc(I);
  until I >= ASeconds;
  finally
    FInLoop := False;
  end;
end;

end.

