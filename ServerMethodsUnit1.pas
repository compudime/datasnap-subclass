unit ServerMethodsUnit1;

interface

uses System.SysUtils, System.Classes, System.Json, Datasnap.DSServer, Datasnap.DSAuth, uHelperUnit,uHelperUnit2;

type
{$METHODINFO ON}
  TServerMethods1 = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
  public
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function CallSleeper(Value: string): string;
    function SleepAndEcho(EchoString, SleepValue: string): string;
  end;
{$METHODINFO OFF}

function MyHelperUnit: THelperUnit;
function MyHelperUnit2: THelperUnit2;

implementation


{$R *.dfm}


uses System.StrUtils;

var
  __MyHelperUnit: THelperUnit;
  __MyHelperUnit2: THelperUnit2;

function MyHelperUnit: THelperUnit;
begin
  if not Assigned(__MyHelperUnit) then
    __MyHelperUnit := THelperUnit.Create;
  Result := __MyHelperUnit;
end;

function MyHelperUnit2: THelperUnit2;
begin
  if not Assigned(__MyHelperUnit2) then
    __MyHelperUnit2 := THelperUnit2.Create;
  Result := __MyHelperUnit2;
end;

function TServerMethods1.CallSleeper(Value: string): string;
var
  ASeconds: Integer;
begin
  ASeconds := StrToInt(Value);
  try
    MyHelperUnit.WaitSomeTime(ASeconds);
    Result := 'GREAT';
  except
    on E: Exception do
      Result := 'Error: ' + E.Message;
  end;
end;

procedure TServerMethods1.DataModuleDestroy(Sender: TObject);
begin
  if Assigned(__MyHelperUnit) then
    FreeAndNil(__MyHelperUnit);
  if Assigned(__MyHelperUnit2) then
    FreeAndNil(__MyHelperUnit2);
end;

function TServerMethods1.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;

function TServerMethods1.SleepAndEcho(EchoString, SleepValue: string): string;
var
  ASeconds: Integer;
begin
  ASeconds := StrToInt(SleepValue);
  try
    MyHelperUnit2.WaitAndEcho(EchoString, ASeconds);
    Result := MyHelperUnit2.MyVar;
  except
    on E: Exception do
      Result := 'Error: ' + E.Message;
  end;
end;

end.

